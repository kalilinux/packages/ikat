var MAX_OUTPUT = 15
var output = Array();

function print(s) {
    // add s to the output array
    output.push(s);
    if (output.length > MAX_OUTPUT) {
        output.shift();
    }
    var o = output.join('\n');

    // set text in TEXTAREA
    var console_output = document.getElementById('console_output');
    console_output.value = o;
    // don't work in Opera?
    console_output.scrollTop = 9999;
}

function run() {
    var console_input = document.getElementById('console_input');
    var command = console_input.value;
    print('>> ' + command);
    var r = null;
    try {
        r = eval(command);
    }
    catch (e) {
        alert(e.message, e.description);
    }
    if (r != null) {
        print(r.toString());
    }
    console_input.focus();
    console_input.select();
    return false;
}

// insert HTML form
var FORM = "\
<form onsubmit='return run()'>\
  <textarea id='console_output' cols='50' rows='15' >\
  </textarea>\
  <br><p><strong>Javascript To Eval</strong><br> <strong><input id='console_input' type='text' value='alert(document.cookie)' size='45' />\
  <input type='submit' value='Eval' xonclick='javascript:run();'/>\
  </p>\
</form></strong>\
";
document.write(FORM);
