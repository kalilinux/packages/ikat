// This part opens and closes content areas on the home page, so that it looks cool.
// Scroll down for the navigational links inside the boxes.
$(document).ready(function() {
	$("#box1_toggle").click(function() {
		
		$("#box1").toggleClass("brick_wide");
		$("#box1_open").toggle();
		$("#box1_close").toggle();
		$("#box1_nav").toggle();
		$("#box1_content").toggle();
		return false;
	});
	$("#box2_toggle").click(function() {
		
		$("#box2").toggleClass("brick_wide");
		$("#box2_open").toggle();
		$("#box2_close").toggle();
		$("#box2_nav").toggle();
		$("#box2_content").toggle();
		return false;
	});
	$("#box3_toggle").click(function() {
		
		$("#box3").toggleClass("brick_wide");
		$("#box3_open").toggle();
		$("#box3_close").toggle();
		$("#box3_nav").toggle();
		$("#box3_content").toggle();
		return false;
	});
	$("#box4_toggle").click(function() {
		
		$("#box4").toggleClass("brick_wide");
		$("#box4_open").toggle();
		$("#box4_close").toggle();
		$("#box4_nav").toggle();
		$("#box4_content").toggle();
		return false;
	});
	$("#box5_toggle").click(function() {
		
		$("#box5").toggleClass("brick_wide");
		$("#box5_open").toggle();
		$("#box5_close").toggle();
		$("#box5_nav").toggle();
		$("#box5_content").toggle();
		return false;
	});
	$("#box6_toggle").click(function() {
		
		$("#box6").toggleClass("brick_wide");
		$("#box6_open").toggle();
		$("#box6_close").toggle();
		$("#box6_nav").toggle();
		$("#box6_content").toggle();
		return false;
	});
	$("#box7_toggle").click(function() {
		
		$("#box7").toggleClass("brick_wide");
		$("#box7_open").toggle();
		$("#box7_close").toggle();
		$("#box7_nav").toggle();
		$("#box7_content").toggle();
		return false;
	});
	$("#box8_toggle").click(function() {
		
		$("#box8").toggleClass("brick_wide");
		$("#box8_open").toggle();
		$("#box8_close").toggle();
		$("#box8_nav").toggle();
		$("#box8_content").toggle();
		return false;
	});
	$("#box9_toggle").click(function() {
		
		$("#box9").toggleClass("brick_wide");
		$("#box9_open").toggle();
		$("#box9_close").toggle();
		$("#box9_nav").toggle();
		$("#box9_content").toggle();
		return false;
	});
	
		$("#box0_toggle").click(function() {
		$("#box0").toggleClass("brick_wide");
		$("#box0_open").toggle();
		$("#box0_close").toggle();
		$("#box0_nav").toggle();
		$("#box0_content").toggle();
		return false;
	});
	
		$("#box10_toggle").click(function() {
		$("#box10").toggleClass("brick_wide");
		$("#box10_open").toggle();
		$("#box10_close").toggle();
		$("#box10_nav").toggle();
		$("#box10_content").toggle();
		return false;
	});
	
// This part shows and hides various functions. 
// There's probably a much, much more elegant way to do this	

	//BOX 1 TOGGLES
	$("#box1_nav1_toggle").click(function() {
		$("#box1 .inner-function").hide();
		$("#box1 .content_prompt").hide();
		$("#box1_nav1_content").show();
		return false;
	});
	$("#box1_nav2_toggle").click(function() {
		$("#box1 .inner-function").hide();
		$("#box1 .content_prompt").hide();
		$("#box1_nav2_content").show();
		return false;
	});
	// Skipped Nav 3 because it was just a link to the Flash site
	$("#box1_nav4_toggle").click(function() {
		$("#box1 .inner-function").hide();
		$("#box1 .content_prompt").hide();
		$("#box1_nav4_content").show();
		return false;
	});
	
	// BOX 2 TOGGLES
	$("#box2_nav1_toggle").click(function() {
		$("#box2 .inner-function").hide();
		$("#box2 .content_prompt").hide();
		$("#box2_nav1_content").show();
		return false;
	});
	$("#box2_nav2_toggle").click(function() {
		$("#box2 .inner-function").hide();
		$("#box2 .content_prompt").hide();
		$("#box2_nav2_content").show();
		return false;
	});
	$("#box2_nav3_toggle").click(function() {
		$("#box2 .inner-function").hide();
		$("#box2 .content_prompt").hide();
		$("#box2_nav3_content").show();
		return false;
	});
		$("#box2_nav4_toggle").click(function() {
		$("#box2 .inner-function").hide();
		$("#box2 .content_prompt").hide();
		$("#box2_nav4_content").show();
		return false;
	});
		$("#box2_nav5_toggle").click(function() {
		$("#box2 .inner-function").hide();
		$("#box2 .content_prompt").hide();
		$("#box2_nav5_content").show();
		return false;
	});
	
	//BOX 3 TOGGLES
	$("#box3_nav1_toggle").click(function() {
		$("#box3 .inner-function").hide();
		$("#box3 .content_prompt").hide();
		$("#box3_nav1_content").show();
		return false;
	});
	$("#box3_nav2_toggle").click(function() {
		$("#box3 .inner-function").hide();
		$("#box3 .content_prompt").hide();
		$("#box3_nav2_content").show();
		return false;
	});
	$("#box3_nav3_toggle").click(function() {
		$("#box3 .inner-function").hide();
		$("#box3 .content_prompt").hide();
		$("#box3_nav3_content").show();
		return false;
	});
		$("#box3_nav4_toggle").click(function() {
		$("#box3 .inner-function").hide();
		$("#box3 .content_prompt").hide();
		$("#box3_nav4_content").show();
		return false;
	});
		$("#box3_nav5_toggle").click(function() {
		$("#box3 .inner-function").hide();
		$("#box3 .content_prompt").hide();
		$("#box3_nav5_content").show();
		return false;
	});
	
	//BOX 4 TOGGLES
	$("#box4_nav1_toggle").click(function() {
		$("#box4 .inner-function").hide();
		$("#box4 .content_prompt").hide();
		$("#box4_nav1_content").show();
		return false;
	});
	$("#box4_nav2_toggle").click(function() {
		$("#box4 .inner-function").hide();
		$("#box4 .content_prompt").hide();
		$("#box4_nav2_content").show();
		return false;
	});
	$("#box4_nav3_toggle").click(function() {
		$("#box4 .inner-function").hide();
		$("#box4 .content_prompt").hide();
		$("#box4_nav3_content").show();
		return false;
	});
		$("#box4_nav4_toggle").click(function() {
		$("#box4 .inner-function").hide();
		$("#box4 .content_prompt").hide();
		$("#box4_nav4_content").show();
		return false;
	});
		$("#box4_nav5_toggle").click(function() {
		$("#box4 .inner-function").hide();
		$("#box4 .content_prompt").hide();
		$("#box4_nav5_content").show();
		return false;
	});
	
	
	//BOX 5 TOGGLES
	$("#box5_nav1_toggle").click(function() {
		$("#box5 .inner-function").hide();
		$("#box5 .content_prompt").hide();
		$("#box5_nav1_content").show();
		return false;
	});
	$("#box5_nav2_toggle").click(function() {
		$("#box5 .inner-function").hide();
		$("#box5 .content_prompt").hide();
		$("#box5_nav2_content").show();
		return false;
	});
	$("#box5_nav3_toggle").click(function() {
		$("#box5 .inner-function").hide();
		$("#box5 .content_prompt").hide();
		$("#box5_nav3_content").show();
		return false;
	});
		$("#box5_nav4_toggle").click(function() {
		$("#box5 .inner-function").hide();
		$("#box5 .content_prompt").hide();
		$("#box5_nav4_content").show();
		return false;
	});
		$("#box5_nav5_toggle").click(function() {
		$("#box5 .inner-function").hide();
		$("#box5 .content_prompt").hide();
		$("#box5_nav5_content").show();
		return false;
	});
	
	
	//BOX 6 TOGGLES
	$("#box6_nav1_toggle").click(function() {
		$("#box6 .inner-function").hide();
		$("#box6 .content_prompt").hide();
		$("#box6_nav1_content").show();
		return false;
	});
	$("#box6_nav2_toggle").click(function() {
		$("#box6 .inner-function").hide();
		$("#box6 .content_prompt").hide();
		$("#box6_nav2_content").show();
		return false;
	});
	$("#box6_nav3_toggle").click(function() {
		$("#box6 .inner-function").hide();
		$("#box6 .content_prompt").hide();
		$("#box6_nav3_content").show();
		return false;
	});
		$("#box6_nav4_toggle").click(function() {
		$("#box6 .inner-function").hide();
		$("#box6 .content_prompt").hide();
		$("#box6_nav4_content").show();
		return false;
	});
		$("#box6_nav5_toggle").click(function() {
		$("#box6 .inner-function").hide();
		$("#box6 .content_prompt").hide();
		$("#box6_nav5_content").show();
		return false;
	});
	
	//BOX 7 TOGGLES
	$("#box7_nav1_toggle").click(function() {
		$("#box7 .inner-function").hide();
		$("#box7 .content_prompt").hide();
		$("#box7_nav1_content").show();
		return false;
	});
	$("#box7_nav2_toggle").click(function() {
		$("#box7 .inner-function").hide();
		$("#box7 .content_prompt").hide();
		$("#box7_nav2_content").show();
		return false;
	});
	$("#box7_nav3_toggle").click(function() {
		$("#box7 .inner-function").hide();
		$("#box7 .content_prompt").hide();
		$("#box7_nav3_content").show();
		return false;
	});
		$("#box7_nav4_toggle").click(function() {
		$("#box7 .inner-function").hide();
		$("#box7 .content_prompt").hide();
		$("#box7_nav4_content").show();
		return false;
	});
		$("#box7_nav5_toggle").click(function() {
		$("#box7 .inner-function").hide();
		$("#box7 .content_prompt").hide();
		$("#box7_nav5_content").show();
		return false;
	});
	
	//BOX 8 TOGGLES
	$("#box8_nav1_toggle").click(function() {
		$("#box8 .inner-function").hide();
		$("#box8 .content_prompt").hide();
		$("#box8_nav1_content").show();
		return false;
	});
	$("#box8_nav2_toggle").click(function() {
		$("#box8 .inner-function").hide();
		$("#box8 .content_prompt").hide();
		$("#box8_nav2_content").show();
		return false;
	});
	$("#box8_nav3_toggle").click(function() {
		$("#box8 .inner-function").hide();
		$("#box8 .content_prompt").hide();
		$("#box8_nav3_content").show();
		return false;
	});
		$("#box8_nav8_toggle").click(function() {
		$("#box8 .inner-function").hide();
		$("#box8 .content_prompt").hide();
		$("#box8_nav4_content").show();
		return false;
	});
		$("#box8_nav5_toggle").click(function() {
		$("#box8 .inner-function").hide();
		$("#box8 .content_prompt").hide();
		$("#box8_nav5_content").show();
		return false;
	});
	
	//BOX 9 TOGGLES
	$("#box9_nav1_toggle").click(function() {
		$("#box9 .inner-function").hide();
		$("#box9 .content_prompt").hide();
		$("#box9_nav1_content").show();
		return false;
	});
	$("#box9_nav2_toggle").click(function() {
		$("#box9 .inner-function").hide();
		$("#box9 .content_prompt").hide();
		$("#box9_nav2_content").show();
		return false;
	});
	$("#box9_nav3_toggle").click(function() {
		$("#box9 .inner-function").hide();
		$("#box9 .content_prompt").hide();
		$("#box9_nav3_content").show();
		return false;
	});
		$("#box9_nav4_toggle").click(function() {
		$("#box9 .inner-function").hide();
		$("#box9 .content_prompt").hide();
		$("#box9_nav4_content").show();
		return false;
	});
		$("#box9_nav5_toggle").click(function() {
		$("#box9 .inner-function").hide();
		$("#box9 .content_prompt").hide();
		$("#box9_nav5_content").show();
		return false;
	});

	//BOX 0 TOGGLES
	$("#box0_nav1_toggle").click(function() {
		$("#box0 .inner-function").hide();
		$("#box0 .content_prompt").hide();
		$("#box0_nav1_content").show();
		return false;
	});
	$("#box0_nav2_toggle").click(function() {
		$("#box0 .inner-function").hide();
		$("#box0 .content_prompt").hide();
		$("#box0_nav2_content").show();
		return false;
	});
	$("#box0_nav3_toggle").click(function() {
		$("#box0 .inner-function").hide();
		$("#box0 .content_prompt").hide();
		$("#box0_nav3_content").show();
		return false;
	});
		$("#box0_nav4_toggle").click(function() {
		$("#box0 .inner-function").hide();
		$("#box0 .content_prompt").hide();
		$("#box0_nav4_content").show();
		return false;
	});
		$("#box0_nav5_toggle").click(function() {
		$("#box0 .inner-function").hide();
		$("#box0 .content_prompt").hide();
		$("#box0_nav5_content").show();
		return false;
	});

	//BOX 10 TOGGLES
	$("#box10_nav1_toggle").click(function() {
		$("#box10 .inner-function").hide();
		$("#box10 .content_prompt").hide();
		$("#box10_nav1_content").show();
		return false;
	});
	$("#box10_nav2_toggle").click(function() {
		$("#box10 .inner-function").hide();
		$("#box10 .content_prompt").hide();
		$("#box10_nav2_content").show();
		return false;
	});
	  $("#box10_nav3_toggle").click(function() {
		$("#box10 .inner-function").hide();
		$("#box10 .content_prompt").hide();
		$("#box10_nav3_content").show();
		return false;
	});
		$("#box10_nav4_toggle").click(function() {
		$("#box10 .inner-function").hide();
		$("#box10 .content_prompt").hide();
		$("#box10_nav4_content").show();
		return false;
	});
		$("#box10_nav5_toggle").click(function() {
		$("#box10 .inner-function").hide();
		$("#box10 .content_prompt").hide();
		$("#box10_nav5_content").show();
		return false;
	});

});
