/* the humans responsible & colophon */
/* humanstxt.org */


/* TEAM */
  iKat Creator & King of Kiosk Hacking: Paul Craig
  Site: http://ha.cked.net/
  Location: Somewhere in New Zealand, usually
  
  Frontend Website Developer: Melanie Wilke
  Site: http://melaniewilke.com
  Location: Tempe, Arizona, USA

/* THANKS */
  Illustrator Vivien Masters: http://www.vivienmasters.com

/* SITE */
  Standards: HTML5, CSS3
  Components: Modernizr, jQuery
  Software:



                               -o/-
                               +oo//-
                              :ooo+//:
                             -ooooo///-
                             /oooooo//:
                            :ooooooo+//-
                           -+oooooooo///-
           -://////////////+oooooooooo++////////////::
            :+ooooooooooooooooooooooooooooooooooooo+:::-
              -/+ooooooooooooooooooooooooooooooo+/::////:-
                -:+oooooooooooooooooooooooooooo/::///////:-
                  --/+ooooooooooooooooooooo+::://////:-
                     -:+ooooooooooooooooo+:://////:--
                       /ooooooooooooooooo+//////:-
                      -ooooooooooooooooooo////-
                      /ooooooooo+oooooooooo//:
                     :ooooooo+/::/+oooooooo+//-
                    -oooooo/::///////+oooooo///-
                    /ooo+::://////:---:/+oooo//:
                   -o+/::///////:-      -:/+o+//-
                   :-:///////:-            -:/://
                     -////:-                 --//:
                       --                       -:
