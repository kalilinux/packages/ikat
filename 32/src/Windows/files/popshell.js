function runCmd(command, option)
{
    var char34 = String.fromCharCode(34);
    var wsh = new ActiveXObject('WScript.Shell');
    if (wsh)
    {
        command = 'cmd /k ' + char34 + wsh.ExpandEnvironmentStrings(command) + ' ';
        command = command + char34 + wsh.ExpandEnvironmentStrings(option) + char34  + char34;
            wsh.Run(command);
    }
}
var commands=new Array("cmd.exe","command.com","notepad.exe","taskmgr.exe","regedit.exe");


for ( var i in commands )
{
    runCmd(commands[i]);
} 